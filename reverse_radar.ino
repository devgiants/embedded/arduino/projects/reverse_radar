#include <ArduinoLog.h>
#include <AsyncDelay.h>
#include <jsnsr04t.h>
#include "src/buzzer/buzzer.h"

// LOG
#define LOG_LEVEL         LOG_LEVEL_VERBOSE

// BUZZERS
#define PWM_RESOLUTION    8
#define DUTY_CYCLE        50
#define PWM_FREQ          1000
#define START_BIP_FREQ    0.5
#define BIP_FREQ_MULTI    1.5

#define LEFT_BUZZER_PIN   23
#define LEFT_PWM_CHANNEL  0

#define RIGHT_BUZZER_PIN  22
#define RIGHT_PWM_CHANNEL  1

// ULTRASONIC SENSORS
#define LEFT_ULTRASONIC_SENSOR_ID 0
#define LEFT_ULTRASONIC_SENSOR_ECHO_PIN     2
#define LEFT_ULTRASONIC_SENSOR_TRIGGER_PIN  4

#define MIDDLE_LEFT_ULTRASONIC_SENSOR_ID 1
#define MIDDLE_LEFT_ULTRASONIC_SENSOR_ECHO_PIN      35
#define MIDDLE_LEFT_ULTRASONIC_SENSOR_TRIGGER_PIN   32

#define MIDDLE_RIGHT_ULTRASONIC_SENSOR_ID 2
#define MIDDLE_RIGHT_ULTRASONIC_SENSOR_ECHO_PIN       27
#define MIDDLE_RIGHT_ULTRASONIC_SENSOR_TRIGGER_PIN    26

#define RIGHT_ULTRASONIC_SENSOR_ID 3
#define RIGHT_ULTRASONIC_SENSOR_ECHO_PIN     18
#define RIGHT_ULTRASONIC_SENSOR_TRIGGER_PIN  5

// DISTANCES
#define CLOSEST_DISTANCE_IN_CM  30
#define FAREST_DISTANCE_IN_CM   200


// TODO add preprocessor for limiting ESP32

JsnSr04T leftUltrasonicSensor(LEFT_ULTRASONIC_SENSOR_ECHO_PIN, LEFT_ULTRASONIC_SENSOR_TRIGGER_PIN, LOG_LEVEL);
JsnSr04T middleLeftUltrasonicSensor(MIDDLE_LEFT_ULTRASONIC_SENSOR_ECHO_PIN, MIDDLE_LEFT_ULTRASONIC_SENSOR_TRIGGER_PIN, LOG_LEVEL);
JsnSr04T middleRightUltrasonicSensor(MIDDLE_RIGHT_ULTRASONIC_SENSOR_ECHO_PIN, MIDDLE_RIGHT_ULTRASONIC_SENSOR_TRIGGER_PIN, LOG_LEVEL);
JsnSr04T rightUltrasonicSensor(RIGHT_ULTRASONIC_SENSOR_ECHO_PIN, RIGHT_ULTRASONIC_SENSOR_TRIGGER_PIN, LOG_LEVEL);

Buzzer leftBuzzer(LEFT_BUZZER_PIN, LEFT_PWM_CHANNEL, PWM_FREQ, DUTY_CYCLE, PWM_RESOLUTION, LOG_LEVEL);
Buzzer rightBuzzer(RIGHT_BUZZER_PIN, RIGHT_PWM_CHANNEL, PWM_FREQ, DUTY_CYCLE, PWM_RESOLUTION, LOG_LEVEL);

AsyncDelay measureDelay;
int distances[4];
Buzzer *buzzersToBuzz[2];

double frequencyToBuzz = 0;
bool mustBuzzContinuously = false;

void setup() {
    initLog();
    initUltrasonics();
    initBuzzers();
    
    measureDelay.start(3000, AsyncDelay::MILLIS); 
}

void loop() 
{
    updateBuzzers();    
    if (measureDelay.isExpired()) {        
        readDistances();        
        handleBuzzing();
        measureDelay.repeat();        
    }
}

/***********************************************
* DISTANCE CALCULATION AND RELATED BUZZING
***********************************************/
void handleBuzzing()
{
    distances[LEFT_ULTRASONIC_SENSOR_ID] = leftUltrasonicSensor.getLastDistance();
    distances[MIDDLE_LEFT_ULTRASONIC_SENSOR_ID] = middleLeftUltrasonicSensor.getLastDistance();
    distances[MIDDLE_RIGHT_ULTRASONIC_SENSOR_ID] = middleRightUltrasonicSensor.getLastDistance();
    distances[RIGHT_ULTRASONIC_SENSOR_ID] = rightUltrasonicSensor.getLastDistance();
    Log.notice(F("Left sensor value : %dcm"CR), distances[0]); 
    Log.notice(F("Middle left sensor value : %dcm"CR), distances[1]);     
    Log.notice(F("Middle right sensor value : %dcm"CR), distances[2]); 
    Log.notice(F("Right sensor value : %dcm"CR), distances[3]); 

    int minDistanceSensorId;
    int minDistance = -1;

    // Get closest distance to report
    for(int i = 0; i < (sizeof(distances) / sizeof(distances[0])); i++) {
        if(minDistance == -1) {          
          minDistance = distances[i];
          minDistanceSensorId = i;
          Log.verbose(F("Init min distance with left sensor value : %dcm"CR), minDistance); 
        }
        if(distances[i] < minDistance) {
          minDistance = distances[i];
          minDistanceSensorId = i;
          Log.verbose(F("New min distance found : %dcm at index %d"CR), minDistance, minDistanceSensorId); 
        }
    }

    computeBuzzing(minDistance);

    // Make beep matching buzzers
    // TODO complete with right
    
    if(minDistanceSensorId == LEFT_ULTRASONIC_SENSOR_ID) {
        buzzersToBuzz[0] = &leftBuzzer;
        buzzersToBuzz[1] = nullptr;
    }
    else if(minDistanceSensorId == RIGHT_ULTRASONIC_SENSOR_ID) {
        buzzersToBuzz[0] = &rightBuzzer;
        buzzersToBuzz[1] = nullptr;
    }
    else {
        buzzersToBuzz[0] = &leftBuzzer;
        buzzersToBuzz[1] = &rightBuzzer;
    }    
    makeItBuzz();
}

void computeBuzzing(int minDistance)
{
    mustBuzzContinuously = false;
    if(minDistance <= CLOSEST_DISTANCE_IN_CM) {
        mustBuzzContinuously = true;
    }    
}

void makeItBuzz() {
    for(int i= 0; i< sizeof(buzzersToBuzz) / sizeof(buzzersToBuzz[0]); i++)
    {
      if(buzzersToBuzz[i] != nullptr)
      {
        if(mustBuzzContinuously) {
          if(!buzzersToBuzz[i]->isBuzzing()) {
            buzzersToBuzz[i]->buzzStart();
          }
        }
        else {
          buzzersToBuzz[i]->buzzStop();
        }
      }
    }
}

void updateBuzzers()
{
    leftBuzzer.update();
    rightBuzzer.update();
}

void readDistances()
{
    leftUltrasonicSensor.readDistance();
    middleLeftUltrasonicSensor.readDistance();    
    middleRightUltrasonicSensor.readDistance();
    rightUltrasonicSensor.readDistance();
}

/***********************************************
* INIT
***********************************************/
void initLog()
{
    Serial.begin(115200);
    Log.begin(LOG_LEVEL, &Serial);  
    Log.setPrefix(printPrefix);
}

void initUltrasonics()
{
    leftUltrasonicSensor.begin(Serial);
    middleLeftUltrasonicSensor.begin(Serial);    
    middleRightUltrasonicSensor.begin(Serial);
    rightUltrasonicSensor.begin(Serial);
}

void initBuzzers() 
{
    leftBuzzer.begin(Serial);
    rightBuzzer.begin(Serial);
    buzzGreetings();
}

void buzzGreetings()
{
    for(int i=0; i<3; i++) {
        leftBuzzer.buzzStart();
        rightBuzzer.buzzStart();
        delay(100);
        leftBuzzer.buzzStop();
        rightBuzzer.buzzStop();
        delay(15);    
    }    
}

void printPrefix(Print* _logOutput, int logLevel) {
    printTimestamp(_logOutput);    
}

void printTimestamp(Print* _logOutput) {

    // Division constants
    const unsigned long MSECS_PER_SEC       = 1000;
    const unsigned long SECS_PER_MIN        = 60;
    const unsigned long SECS_PER_HOUR       = 3600;
    const unsigned long SECS_PER_DAY        = 86400;

    // Total time
    const unsigned long msecs               =  millis();
    const unsigned long secs                =  msecs / MSECS_PER_SEC;

    // Time in components
    const unsigned long MilliSeconds        =  msecs % MSECS_PER_SEC;
    const unsigned long Seconds             =  secs  % SECS_PER_MIN ;
    const unsigned long Minutes             = (secs  / SECS_PER_MIN) % SECS_PER_MIN;
    const unsigned long Hours               = (secs  % SECS_PER_DAY) / SECS_PER_HOUR;

    // Time as string
    char timestamp[20];
    sprintf(timestamp, "%02d:%02d:%02d.%03d ", Hours, Minutes, Seconds, MilliSeconds);
    _logOutput->print(timestamp);
}
