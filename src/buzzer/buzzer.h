#ifndef BUZZER_H
    #define BUZZER_H
    #include <ArduinoLog.h>
    #include <AsyncDelay.h>
    #include "Arduino.h"


    class Buzzer
    {
        public:
            Buzzer(int pin, int pwmChannel, int frequency, int dutyCycle, int resolution, int logLevel = LOG_LEVEL_VERBOSE);
            void begin(Stream &serial);
            void update();
            void buzzWhile(int milliseconds);
            void buzzStart();
            void buzzStop();
            void buzzAtFrequency(double buzzFrequency);
            void stopBuzzAtFrequency();
            bool isBuzzing();
        private:
            int _pin;
            int _pwmChannel;
            int _toneFrequency;
            int _dutyCycle;
            int _resolution;
            int _logLevel;
            bool _isBuzzing;
            double _currentBuzzingPeriod;
            Stream* _serial;
            AsyncDelay _buzzLow;
            AsyncDelay _buzzHigh;
            void printPrefix();
    };
#endif