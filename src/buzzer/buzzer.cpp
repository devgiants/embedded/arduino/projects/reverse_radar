#include "buzzer.h"

Buzzer::Buzzer(int pin, int pwmChannel, int toneFrequency, int dutyCycle, int resolution, int logLevel)
{
    _pin = pin;
    _pwmChannel = pwmChannel;
    _toneFrequency = toneFrequency;
    _dutyCycle = dutyCycle;
    _resolution = resolution;
    _logLevel = logLevel;
    _isBuzzing = false;
    _currentBuzzingPeriod = 0;
}

void Buzzer::begin(Stream &serial)
{
    _serial = &serial;
    Log.begin(_logLevel, _serial);    
    double dutyCycleInPercent = (_dutyCycle / pow(2, _resolution))*100;
    Log.verbose(
        F(
            "Create buzzer with following params : pin %d, pwmChannel %d, tone frequency %dHz, duty cycle %D%%"CR), 
            _pin,
            _pwmChannel,
            _toneFrequency,
            dutyCycleInPercent     
        );
    ledcSetup(_pwmChannel, _toneFrequency, _resolution);
    ledcAttachPin(_pin, _pwmChannel);
}

void Buzzer::update()
{
    if(_currentBuzzingPeriod > 0) {
        if(_buzzHigh.isExpired() && this->_isBuzzing) {        
            this->buzzStop();        
            // Divided by 2 because period is signal wide. Down time is half this value
            // Duty cycle is 50% here
            _buzzLow.start(_currentBuzzingPeriod * 1000 /2, AsyncDelay::MILLIS);        
        }
        if(_buzzLow.isExpired() && !this->_isBuzzing) {
            // Divided by 2 because period is signal wide. Up time is half this value
            // Duty cycle is 50% here
            this->buzzWhile(_currentBuzzingPeriod * 1000 /2);
        }
    }
}

void Buzzer::buzzWhile(int milliseconds)
{
    Log.notice(F("Ask for buzzing on pin %d and PWM channel %d during %dms"CR), _pin, _pwmChannel, milliseconds);
    this->buzzStart();
    _buzzHigh.start(milliseconds, AsyncDelay::MILLIS);
}

void Buzzer::buzzStart()
{
    Log.notice(F("Buzz START on pin %d and PWM channel %d"CR), _pin, _pwmChannel);
    ledcWriteTone(_pwmChannel, _toneFrequency);
    this->_isBuzzing = true;
}
void Buzzer::buzzStop()
{
    Log.notice(F("Buzz STOP on pin %d and PWM channel %d"CR), _pin, _pwmChannel);
    ledcWrite(_pwmChannel, 0);
    this->_isBuzzing = false;
}

void Buzzer::buzzAtFrequency(double buzzFrequency)
{
    Log.notice(F("Ask for buzzing at %DDHz frequency"CR), buzzFrequency);
    double newPeriod = 1/buzzFrequency;    
    Log.verbose(F("Period found from %DHz : %Ds"CR), buzzFrequency, newPeriod);


    // If buzzer wasn't in frequency mode, start it
    if(_currentBuzzingPeriod == 0) {
        // Divided by 2 because perdio is signal wide. Up time is half this value
        // Duty cycle is 50% here
        _currentBuzzingPeriod = newPeriod;
        this->buzzWhile(_currentBuzzingPeriod * 1000/2);
    }

    // If it was in frequency mode, just change the period. On next cycle it will be taken.
    else {
        _currentBuzzingPeriod = newPeriod;
    }
}

void Buzzer::stopBuzzAtFrequency()
{
    Log.notice(F("Ask for stopping repeating buzz"CR));
    _currentBuzzingPeriod = 0;    
}

bool Buzzer::isBuzzing()
{
    return this->_isBuzzing;
}